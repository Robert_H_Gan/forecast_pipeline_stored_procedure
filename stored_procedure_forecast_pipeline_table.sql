-- stored procedure for the forecast pipeline table

USE [DW]
GO

/****** Object:  StoredProcedure [dbo].[PULL_DWBI_MART_OPPORTUNITY_HISTORY]    Script Date: 5/7/2021 10:12:33 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[PULL_DWBI_MART_OPPORTUNITY_HISTORY]
AS

declare @dwid_pull int
select @dwid_pull = (select max(dwid_pull) from dw_pull)


declare @sys int
select @sys = 10

declare @ret_status tinyint
select @ret_status = 0 

declare @rowcount int


insert into dw_audit (dwid_pull,name_object,dwid_sys)
select @dwid_pull, 'mart_opportunity_pipeline_history_dev', @sys

insert into dw_audit (dwid_pull,name_object,dwid_sys)
select @dwid_pull, 'mart_opportunity_stage_progression_history_dev', @sys



-- new opportunity_history_table---------------------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists #user_table;
SELECT 
    [SID_Id] as owner_id, 
    [EVP__c] as sales_user_svp,
    FirstName as sales_user_first_name,
    LastName as sales_user_last_name,
    CVP__c as sales_user_cvp,
    AVP__c as sales_user_avp,
    RVP__c as sales_user_rvp,
    DIR__c as sales_user_dir,
    case 
        when EVP__c in ('Patrick Manzo', 'Robert Hartsough') then 'Total Content' 
        when EVP__c in ('Ted Winslow','Liam Butler', 'Tony Barbone') then 'Sumtotal' 
        else 'Other' 
    end as svp_based_product, 
    case 
        when EVP__c in ('Ted Winslow','Liam Butler') then 'Sumtotal'
        when cvp__c in('Agata Nowakowska','Kamal Dutta','Rosie Cairnes') then 'Rest of World' 
        when rvp__c in ('Rhys Hughes','Rajiv Kumar','Ian Rawlings') then 'Rest of World'
        else 'North America'
    end as region_rollup, 
    case 
        when cvp__c = 'Agata Nowakowska' then 'EMEA' 
        when cvp__c = 'Kamal Dutta' then 'India' 
        when cvp__c = 'Rosie Cairnes' then 'APAC'
        when [RVP__c] = 'Rhys Hughes' then 'APAC'
        when [RVP__c] = 'Rajiv Kumar' then 'India'
        when [RVP__c] = 'Ian Rawlings' then 'EMEA'
        else 'North America' 
    end as region,
    case 
        when [EVP__c] in ('Patrick Manzo', 'Ted Winslow') then [EVP__c]
        else 'Others'
    end as Pat_vs_Ted_vs_others,  --flags for specific analysis
    case 
        when 
            cvp__c not in ('Patrick Manzo', 'Edward Zaval', 'Steven Wainwright (SS)', 'SS CVP NA Field')
            and avp__c not in ('Gwenn Lazar', 'Wayne Clement')
            and rvp__c not in ('Max Coutinho') 
        then 0 
        else 1 
    end as sales_team_exclusion_flag_sv
into #user_table
FROM [DW].[dbo].[DIM_SF_USER]
where date_end is null
;


create nonclustered index idx_SID_Id on #user_table([owner_id])


drop table if exists #product_table;
select 
    SID_Id,
    product_type__c as forecast_product_type,
    -- case 
    --     when Product_Type__c in ('Prod & Collab', 'A la carte', 'Other', 'Business & Lead', 'Complete Coll') then 1
    --     else 0
    -- end as Skillsoft_BL_flag,
    -- case 
    --     when Product_Type__c in ('Complete Coll', 'Tech & Dev') then 1
    --     else 0
    -- end as Skillsoft_TD_flag,
    -- case 
    --     when Product_Type__c in ('Compliance') then 1
    --     else 0
    -- end as Skillsoft_Compliance_flag,
    -- case 
    --     when Product_Type__c in ('Other Platform', 'Learn-Entrprse', 'Learn-Growth') then 1
    --     else 0
    -- end as SumTotal_flag,
    case 
        when 
            Product_Type__c in ('Compliance', 'Prod & Collab', 'A la carte', 'Other', 'Business & Lead', 'Complete Coll', 'Tech & Dev') -- these are all skillsoft
        then 0
        else 1
    end as forecast_product_exclusion_flag_sv,
    case 
        when Product_Type__c in ('Compliance', 'Prod & Collab', 'A la carte', 'Other', 'Business & Lead', 'Complete Coll', 'Tech & Dev') then 'Skillsoft'-- these are all skillsoft
        when Product_Type__c in ('Other Platform', 'Learn-Entrprse', 'Learn-Growth') then 'SumTotal'-- these are all SumTotal
        else 'Others/PS'
    end as forecast_product_family,
    case 
        when Product_Pillar__c in('Percipio','Skillsoft Pillar','Vodeclic','Default') then Product_Pillar__c
        else 'Other'
    end as forecast_platforms
into #product_table
from dw.dbo.DIM_SF_PRODUCT2 
where date_end is null
;

create nonclustered index idx_sid_id on #product_table([sid_id])

-- drop table if exists #account_table;
-- select
--     Account_Id__c,
--     max(
--         case
--             when Company_Size__c in ('50-250', 'Less than 50', '251-500', '1,001-3,000', '501-1,000', '1,001-2,500', '50 – 250', '0', '1001-3000', '1540', '1600', '100-499', '501-1008', '1500') then 'SMB'
--             when Company_Size__c in ( '3,001-5,000',  '2,501-5,000' ) then 'Enterprise'
--             when Company_Size__c in ('More than 10,000', '5,001-7,500', '7,501-10,000','More than 10,002' ) then 'Enterprise'
--             else 'SMB'
--         end
--     ) as opp_customer_size_bucket
-- INTO
--     #account_table
-- FROM
--     dw.dbo.DIM_SF_ACCOUNT
-- WHERE
--     DATE_END is NULL
-- group BY
--     Account_Id__c
-- ;




drop table if exists #date_table_monthly_and_today;
select
    FULL_DATE,
    cast(FULL_DATE as date) as date_full_date,
    cast(first_day_of_month as date) as first_day_of_month,
    SSOFT_YEAR_NAME as Fiscal_Year,
    SSOFT_PERIOD_NAME as Fiscal_mth,
    SSOFT_FISCAL_YEAR_MTH as Fiscal_year_mth
into #date_table_monthly_and_today
from DW.DBO.DIM_DATE
where 
    (FULL_DATE = cast(GETDATE() as date)
    OR cast(full_date as date) = cast(first_day_of_month as date))
    and cast(FULL_DATE as date) between '2018-02-01' and GETDATE()
;

create nonclustered index idx_full_date on #date_table_monthly_and_today([FULL_DATE])


drop table if exists #opp_history_summary_table;
select 
    *
into #opp_history_summary_table
from 
(
    select 
        opp_table.SID_ID as opportunity_id,
        Opportunity_Number__c as opp_number,
        cast(date_begin as date) as date_begin,
        coalesce(cast(date_end as date), cast(GETDATE()+1 as date)) as date_end, 
        StageName as opp_stage,
        [type] as opp_revenue_type,
        opp_user_table.region as opp_region,
        opp_user_table.region_rollup as opp_region_rollup,
        opp_user_table.[sales_user_svp] as opp_svp,
        opp_user_table.[sales_user_cvp] as opp_cvp,
        RANK() OVER (PARTITION BY opp_table.SID_ID, cast(date_begin as date) ORDER BY date_begin DESC) as Last_row_per_day_indicator
    from dw.dbo.FACT_SF_OPPORTUNITY as opp_table
    left join
        #user_table as opp_user_table
        on opp_table.OwnerId = opp_user_table.owner_ID
    group by opp_table.SID_ID, Opportunity_Number__c, date_begin, date_end, StageName, [type], region, region_rollup, sales_user_cvp, sales_user_svp
) a
where Last_row_per_day_indicator = 1
;

create nonclustered index idx_date_begin on #opp_history_summary_table([date_begin])
create nonclustered index idx_date_end on #opp_history_summary_table([date_end])


-- OPP FORECAST HISTORY value---------------------------------------------------------------------------
drop table if exists #opp_forecast_table;
SELECT
    cast(DATE_BEGIN as date) as date_begin,
    cast(DATE_end as date) as date_end,
    cast(Lastmodifieddate as date) as last_modified_date,
    Opportunity__c as opportunity_id,
    Commission_Type__c as forecast_commission_type,
    Sales_User__c as Sales_User,
    Product__c as Product_id, 
    Credit_Type__c as credit_type,
    Revenue_Type__c as forecast_revenue_type,
    Forecast_Category__c as forecast_category,
    Forecast_Date__c as forecast_date,
    core_amount__c *(
            SELECT
                [RATE_MULT]
            FROM
                [DW].[dbo].[DIM_RT_RATE_TBL] with (nolock)
            WHERE
                SID_RT_TYPE = 'CRRNT'
                AND [SID_FROM_CUR] = CurrencyIsoCode
                AND SID_TO_CUR = 'USD'
                AND DATE_END IS NULL
                AND [SID_EFFDT] = DATEFROMPARTS(
                    CASE
                        WHEN DATEPART(M, GETDATE()) = 1 THEN DATEPART(YYYY, GETDATE()) -1
                        ELSE DATEPART(YYYY, GETDATE())
                    END,
                    01,
                    31
                )
    ) AS core_amount_converted,
    case
        when
            Forecast_Category__c in ('Closed', 'Omitted' ,'Upside' ,'Probable' ,'Commit')
        then 0
        else 1
    end as forecast_category_exclusion_flag,
    case
        when
            Forecast_Category__c in ('Upside' ,'Probable' ,'Commit', 'Pipeline', 'Forecast')
        then 0
        else 1
    end as forecast_category_pipeline_flag,
    case
        when revenue_type__c = 'New Business' then 1
        else 0
    end as forecast_new_business_flag
into #opp_forecast_table
from dw.dbo.FACT_SF_OPPORTUNITY_FORECAST__C opp_forecast_table
where (date_end >= '2018-02-01' or date_end is null)
;

create nonclustered index idx_sales_user on #opp_forecast_table([sales_user])
create nonclustered index idx_product_id on #opp_forecast_table([product_id])


drop table if exists #opp_forecast_table_with_user_and_product;
select 
    #opp_forecast_table.*,
    forecast_user_table.region as forecast_region,
    forecast_user_table.sales_team_exclusion_flag_sv as forecast_sales_team_exclusion_flag,
    forecast_user_table.[Sales_user_SVP] as forecast_svp,
    forecast_user_table.sales_user_cvp as forecast_cvp,
    product_table.forecast_product_family as forecast_product_family,
    product_table.forecast_product_type as forecast_product_type,
    product_table.forecast_platforms as forecast_platforms
into #opp_forecast_table_with_user_and_product
from #opp_forecast_table
left JOIN 
    #user_table as forecast_user_table
ON #opp_forecast_table.Sales_User = forecast_user_table.owner_Id
left join
    #product_table as product_table
on #opp_forecast_table.product_id = product_table.SID_Id
;




-- new opportunity_history_table---------------------------------------------------------------------------------------------------------------------------------------------------------------------
drop table if exists #opportunity_stage_rank;
select 
    opportunity_id,
    opp_number,
    date_begin, 
    date_end,
    opp_stage,
    case 
        when opp_stage = 'Qualification' then 1 
        when opp_stage = 'Discovery' then 2 
        when opp_stage = 'Evaluation' then 3 
        when opp_stage = 'Proposal' then 4 
        when opp_stage = 'Negotiation' then 5 
        when opp_stage = 'Closed Won' then 6
        else 0
    end as opp_stage_order,
    rank() over (partition by opportunity_id order by date_begin) as stage_rank
into #opportunity_stage_rank
from #opp_history_summary_table
;

create nonclustered index idx_opportunity_id on #opportunity_stage_rank([opportunity_id])
create nonclustered index idx_stage_rank on #opportunity_stage_rank([stage_rank])


-- getting opp craetion date from SF_opportunity_history table
drop table if exists #opp_history_creation_date_table;
select 
    opportunity_id,
    min(case when stage_rank = 1 then date_begin end) as opp_creation_date
into #opp_history_creation_date_table
from(
    select 
        opportunityid as opportunity_id, 
        cast(createddate as date) as date_begin, 
        stagename as opp_stage, 
        rank() over (partition by opportunityid order by createddate) as stage_rank
    from 
        dw.dbo.FACT_SF_OPPORTUNITYHISTORY
) a
group by opportunity_id
;

CREATE NONCLUSTERED INDEX idx_opportunity_Id ON #opp_history_creation_date_table (opportunity_Id);

-- drop table if exists #opp_source_and_customer_size_bucket_table;
-- select 
--     opportunity_id, 
--     opp_source,
--     opp_customer_size_bucket
-- into #opp_source_and_customer_size_bucket_table
-- from dwbi.dbo.mart_opportunity_master
-- group by opportunity_id, opp_customer_size_bucket, opp_source
-- ;

drop table if exists #opp_unique_feature_table;
select 
    opportunity_id, 
    opp_source,
    opp_customer_size_bucket,
    opp_revenue_type,
    opp_svp,
    opp_cvp,
    opp_region,
    opp_region_rollup
into #opp_unique_feature_table
from dwbi.dbo.mart_opportunity_master
group by opportunity_id, opp_source, opp_customer_size_bucket, opp_revenue_type, opp_svp, opp_cvp, opp_region, opp_region_rollup 
;

CREATE NONCLUSTERED INDEX IDX_opportunity_Id ON #opp_unique_feature_table (opportunity_Id);

drop table if exists #opportunity_stage_rank_history;
select 
    stage.opportunity_id,
    stage.opp_number,
    stage.date_begin,
    next_stage.date_begin as date_end_with_null,
    coalesce(next_stage.date_begin, cast(GETDATE()+1 as date)) as date_end,

    stage.opp_stage,
    stage.stage_rank,
    stage.opp_stage_order,

    next_stage.opp_stage as next_opp_stage,
    next_stage.stage_rank as next_stage_rank,
    next_stage.opp_stage_order as next_opp_stage_order,

    prev_stage.opp_stage as prev_opp_stage,
    prev_stage.stage_rank as prev_stage_rank,
    prev_stage.opp_stage_order as prev_opp_stage_order

into #opportunity_stage_rank_history
from #opportunity_stage_rank stage
LEFT JOIN #opportunity_stage_rank prev_stage
on stage.opportunity_id = prev_stage.opportunity_id and stage.stage_rank = prev_stage.stage_rank + 1
LEFT JOIN #opportunity_stage_rank next_stage
on stage.opportunity_id = next_stage.opportunity_id and stage.stage_rank = next_stage.stage_rank - 1
;

create nonclustered index idx_opp_stage on #opportunity_stage_rank_history([opp_stage])

create nonclustered index idx_prev_opp_stage on #opportunity_stage_rank_history([prev_opp_stage])



drop table if exists #opp_stage_rank_history_with_stage_grouping;
select
    a.*,
    rank_by_opp_id - rank_by_opp_id_and_stage as [stage_grouping],
    min(date_begin) over (partition by opportunity_id, opp_stage, rank_by_opp_id - rank_by_opp_id_and_stage) as min_date_begin
into #opp_stage_rank_history_with_stage_grouping
from 
(
    select 
        opportunity_id,
        opp_number,
        date_begin,
        date_end,
        date_end_with_null,
        opp_stage,
        opp_stage_order,
        rank() over (partition by opportunity_id order by date_begin) as rank_by_opp_id,
        rank() over (partition by opportunity_id, opp_stage order by date_begin) as rank_by_opp_id_and_stage
    from #opportunity_stage_rank_history
) a
order by opportunity_id, date_begin, date_end
;



drop table if exists #opp_enter_exit_date_table; 
select 
    *,
    dense_rank () over (partition by opportunity_id order by stage_enter_date) as stage_enter_date_rank
into #opp_enter_exit_date_table
from 
(
    select 
        opportunity_id,
        opp_number,
        opp_stage,
        opp_stage_order,
        [stage_grouping],
        min(date_begin) as stage_enter_date, 
        case 
            when max(case when date_end_with_null is null then 1 else 0 end ) = 0
            then max(date_end_with_null)
        end as stage_exit_date,
        max(date_end) as stage_exit_date_or_today
    from 
        #opp_stage_rank_history_with_stage_grouping
    group by opportunity_id, opp_number, opp_stage, opp_stage_order, [stage_grouping]
) a
order by opportunity_id
;

create nonclustered index idx_opportunity_id on #opp_enter_exit_date_table ([opportunity_id])

create nonclustered index idx_stage_enter_date_rank on #opp_enter_exit_date_table ([stage_enter_date_rank])



drop table if exists #opp_enter_exit_date_table_with_prom_flag;
select 
    stage.opportunity_id,
    stage.opp_number,

    stage.opp_stage,
    stage.stage_enter_date, 
    stage.stage_enter_date_rank,
    stage.opp_stage_order,

    stage.stage_exit_date,
    stage.stage_exit_date_or_today,

    next_stage.opp_stage as next_opp_stage,
    coalesce(next_stage.stage_enter_date, cast(GETDATE()+1 as date)) as next_stage_enter_date,
    next_stage.stage_enter_date_rank as next_stage_enter_date_rank,
    next_stage.opp_stage_order as next_opp_stage_order,

    case 
        when stage.opp_stage_order < next_stage.opp_stage_order
        then 1
        else 0
    end as opp_stage_promotion_flag

into #opp_enter_exit_date_table_with_prom_flag
from #opp_enter_exit_date_table stage
LEFT JOIN #opp_enter_exit_date_table next_stage
ON stage.opportunity_id = next_stage.opportunity_id and stage.stage_enter_date_rank = next_stage.stage_enter_date_rank -1
;

create nonclustered index idx_opportunity_id on #opp_enter_exit_date_table_with_prom_flag([opportunity_id])
create nonclustered index idx_stage_enter_date on #opp_enter_exit_date_table_with_prom_flag([stage_enter_date])
create nonclustered index idx_stage_exit_date_or_today on #opp_enter_exit_date_table_with_prom_flag([stage_exit_date_or_today])



-----------------------------------------------------------------------opportunity forecast summary table creation------------------------------------------------------------------------------------------------
drop table if exists dwstage.dbo.temp_opportunity_monthly_history_and_today_table;
CREATE TABLE dwstage.dbo.temp_opportunity_monthly_history_and_today_table(
    [date] date,
    first_day_of_month date,
    opportunity_id nvarchar(18),
    opp_number varchar(30),
    opp_stage nvarchar(40),
    opp_revenue_type nvarchar(40),
    opp_region nvarchar(255),
    opp_region_rollup nvarchar(255),
    opp_svp nvarchar(255),
    opp_cvp nvarchar(255)
)
;
INSERT INTO dwstage.dbo.temp_opportunity_monthly_history_and_today_table
(
    [date],
    first_day_of_month,
    opportunity_id,
    opp_number,
    opp_stage,
    opp_revenue_type,
    opp_region,
    opp_region_rollup,
    opp_svp,
    opp_cvp
)
SELECT 
    date_table.date_FULL_DATE as [date],
    first_day_of_month,
    opportunity_id,
    opp_number,
    opp_stage,
    opp_revenue_type,
    opp_region,
    opp_region_rollup,
    opp_svp,
    opp_cvp
from 
    #date_table_monthly_and_today as date_table
left JOIN #opp_history_summary_table
-- ======================================================== 
ON date_table.FULL_DATE >= #opp_history_summary_table.DATE_BEGIN 
AND date_table.FULL_DATE < #opp_history_summary_table.DATE_END
-- ========================================================
;


CREATE NONCLUSTERED INDEX [IDX_date] 
ON dwstage.dbo.temp_opportunity_monthly_history_and_today_table([date])

CREATE NONCLUSTERED INDEX [IDX_opportunity_id] 
ON dwstage.dbo.temp_opportunity_monthly_history_and_today_table([opportunity_id])



-- opp forecast value summary table creation
drop table if exists dwstage.dbo.temp_monthly_and_current_pipeline_summary;
SELECT
    opportunity_id, 
    DATE_BEGIN,
    cast(coalesce(DATE_END, GETDATE()+1) as date) as DATE_END,
    cast(forecast_date as date) as forecast_date,
    forecast_product_type, 
    forecast_product_family,
    forecast_platforms,
    forecast_revenue_type,
    forecast_commission_type,
    forecast_category,
    forecast_region,
    forecast_cvp,
    forecast_svp,
    forecast_category_pipeline_flag,
    forecast_new_business_flag, 
    forecast_sales_team_exclusion_flag,
    forecast_category_exclusion_flag,
    sum(core_amount_converted) as sum_core_amount_converted
into dwstage.dbo.temp_monthly_and_current_pipeline_summary
from #opp_forecast_table_with_user_and_product
group by 
    Opportunity_id, 
    DATE_BEGIN, 
    DATE_END, 
    forecast_date,
    forecast_product_type,
    forecast_product_family,
    forecast_platforms,
    forecast_revenue_type, 
    forecast_commission_type,
    forecast_category,
    forecast_region,
    forecast_cvp,
    forecast_svp, 
    forecast_category_pipeline_flag,
    forecast_new_business_flag, 
    forecast_sales_team_exclusion_flag, 
    forecast_category_exclusion_flag
;



----------------------------------------------------------------------- forecast monthly history table creation ------------------------------------------------------------------------------------------------
drop table if exists dwstage.dbo.temp_monthly_and_current_pipeline_value;
CREATE TABLE dwstage.dbo.temp_monthly_and_current_pipeline_value(
    date_FULL_DATE date,
    first_day_of_month date,
    opportunity_id NVARCHAR(18), 
    forecast_date date,
    forecast_region VARCHAR(13),
    forecast_cvp NVARCHAR(255),
    forecast_svp NVARCHAR(255),
    forecast_new_business_flag int, 
    forecast_sales_team_exclusion_flag int,
    forecast_category_exclusion_flag int,
    forecast_product_family VARCHAR(9),
    forecast_revenue_type NVARCHAR(255),
    forecast_commission_type NVARCHAR(255),
    forecast_category NVARCHAR(1300),
    forecast_category_pipeline_flag int,
    forecast_product_type NVARCHAR(255),
    forecast_platforms NVARCHAR(255),
    sum_core_amount_converted numeric(38,10)
)
;


INSERT INTO dwstage.dbo.temp_monthly_and_current_pipeline_value
(   date_FULL_DATE,
    first_day_of_month,
    opportunity_id, 
    forecast_date,
    forecast_region,
    forecast_cvp,
    forecast_svp,
    forecast_new_business_flag, 
    forecast_sales_team_exclusion_flag,
    forecast_category_exclusion_flag,
    forecast_product_family,
    forecast_revenue_type,
    forecast_commission_type,
    forecast_category,
    forecast_category_pipeline_flag,
    forecast_product_type,
    forecast_platforms,
    sum_core_amount_converted)
SELECT
    date_table.date_FULL_DATE,
    cast(first_day_of_month as date) as first_day_of_month,
    opportunity_id, 
    forecast_date,
    forecast_region,
    forecast_cvp,
    forecast_svp,
    forecast_new_business_flag, 
    forecast_sales_team_exclusion_flag,
    forecast_category_exclusion_flag,
    forecast_product_family,
    forecast_revenue_type,
    forecast_commission_type,
    forecast_category,
    forecast_category_pipeline_flag,
    forecast_product_type,
    forecast_platforms,
    sum(coalesce(sum_core_amount_converted,0)) as sum_core_amount_converted
from 
    #date_table_monthly_and_today as date_table
left JOIN dwstage.dbo.temp_monthly_and_current_pipeline_summary as forecast_table
-- ======================================================== 
ON date_table.FULL_DATE >= forecast_table.DATE_BEGIN
AND date_table.FULL_DATE < forecast_table.DATE_END
-- ========================================================
group by 
    date_full_date, 
    first_day_of_month, 
    opportunity_id, 
    forecast_date, 
    forecast_region,
    forecast_cvp,
    forecast_svp, 
    forecast_product_type, 
    forecast_platforms,
    forecast_revenue_type, 
    forecast_commission_type, 
    forecast_category, 
    forecast_category_pipeline_flag, 
    forecast_new_business_flag, 
    forecast_sales_team_exclusion_flag, 
    forecast_category_exclusion_flag, 
    forecast_product_family 
;



CREATE NONCLUSTERED INDEX [CS_IDX_date_full_date] 
ON dwstage.dbo.temp_monthly_and_current_pipeline_value([date_full_date])


CREATE NONCLUSTERED INDEX [CS_IDX_opportunity_id] 
ON dwstage.dbo.temp_monthly_and_current_pipeline_value([opportunity_id])



drop table if exists dwstage.dbo.temp_monthly_and_current_pipeline_value_and_stage;
CREATE TABLE dwstage.dbo.temp_monthly_and_current_pipeline_value_and_stage (
    full_date date,
    first_day_of_month date,
    previous_first_day_of_month date,
    opportunity_id NVARCHAR(18), 
    opp_number varchar(30),
    forecast_date date,
    forecast_region VARCHAR(13),
    forecast_cvp NVARCHAR(255),
    forecast_svp NVARCHAR(255),
    opp_svp NVARCHAR(255),
    opp_cvp nvarchar(255),
    opp_region NVARCHAR(255),
    opp_region_rollup NVARCHAR(255),
    opp_revenue_type nvarchar(40),
    forecast_new_business_flag int, 
    forecast_sales_team_exclusion_flag int,
    forecast_category_exclusion_flag int,
    forecast_product_family VARCHAR(9),
    forecast_revenue_type NVARCHAR(255),
    forecast_commission_type NVARCHAR(255),
    forecast_product_type NVARCHAR(255),
    forecast_platforms NVARCHAR(255),
    forecast_category NVARCHAR(1300),
    forecast_category_pipeline_flag int,
    opp_stage NVARCHAR(40),
    forecast_value_amount numeric(38,10)
)
;



INSERT INTO dwstage.dbo.temp_monthly_and_current_pipeline_value_and_stage
(   full_date,
    first_day_of_month,
    previous_first_day_of_month,
    opportunity_id, 
    opp_number,
    forecast_date,
    forecast_region, 
    forecast_cvp,
    forecast_svp,
    opp_svp,
    opp_cvp,
    opp_region,
    opp_region_rollup,
    opp_revenue_type,
    forecast_new_business_flag, 
    forecast_sales_team_exclusion_flag,
    forecast_category_exclusion_flag,
    forecast_product_family,
    forecast_revenue_type,
    forecast_commission_type,
    forecast_product_type,
    forecast_platforms,
    forecast_category,
    forecast_category_pipeline_flag,
    opp_stage,
    forecast_value_amount)
select 
    opp_table.[date] as full_date,
    opp_table.first_day_of_month,
    DATEADD(month, -1, opp_table.first_day_of_month) as previous_first_day_of_month,
    opp_table.opportunity_id, 
    opp_table.opp_number,
    forecast_pipeline_table.forecast_date,
    forecast_pipeline_table.forecast_region, 
    forecast_pipeline_table.forecast_cvp,
    forecast_pipeline_table.forecast_svp,
    opp_table.opp_svp,
    opp_table.opp_cvp,
    opp_table.opp_region,
    opp_table.opp_region_rollup,
    opp_table.opp_revenue_type,
    forecast_pipeline_table.forecast_new_business_flag, 
    forecast_pipeline_table.forecast_sales_team_exclusion_flag,
    forecast_pipeline_table.forecast_category_exclusion_flag,
    forecast_pipeline_table.forecast_product_family,
    forecast_pipeline_table.forecast_revenue_type,
    forecast_pipeline_table.forecast_commission_type,
    forecast_pipeline_table.forecast_product_type,
    forecast_pipeline_table.forecast_platforms,
    forecast_pipeline_table.forecast_category,
    forecast_pipeline_table.forecast_category_pipeline_flag,
    opp_table.opp_stage,
    forecast_pipeline_table.sum_core_amount_converted as forecast_value_amount
from 
    dwstage.dbo.temp_monthly_and_current_pipeline_value as forecast_pipeline_table
right join 
    dwstage.dbo.temp_opportunity_monthly_history_and_today_table as opp_table
ON forecast_pipeline_table.date_full_date = opp_table.[date] and forecast_pipeline_table.opportunity_id = opp_table.opportunity_id
;


CREATE NONCLUSTERED INDEX IDX_date_opp ON dwstage.dbo.temp_monthly_and_current_pipeline_value_and_stage (full_date, opportunity_Id);
CREATE NONCLUSTERED INDEX IDX_opportunity_Id ON dwstage.dbo.temp_monthly_and_current_pipeline_value_and_stage (opportunity_Id);


-- PERMANENT TABLE CREATION AREA========================================================================================================================================================================================================

-- TABLE NAME: dwstage.dbo.temp_opp_monthly_stage_age
-- DESCRIPTION: table recording stage age of all opps on every month snapshot and the current snapshots
drop table if exists dwstage.dbo.temp_opp_monthly_stage_age;
create table dwstage.dbo.temp_opp_monthly_stage_age(
    [date] date,
    opportunity_id nvarchar(18),
    opp_number nvarchar(30),
    opp_stage nvarchar(40),
    stage_age int,
    opp_stage_promotion_flag int,
    stage_enter_date date,
    stage_exit_date date,
    stage_exit_date_or_today date
)
;
INSERT INTO dwstage.dbo.temp_opp_monthly_stage_age(
    [date],
    opportunity_id,
    opp_number,
    opp_stage,
    stage_age,
    opp_stage_promotion_flag,
    stage_enter_date,
    stage_exit_date,
    stage_exit_date_or_today
)
select 
    a.[date],
    a.opportunity_id,
    a.opp_number,
    a.opp_stage,
    datediff(day, b.stage_enter_date, a.[date]) as stage_age,
    b.opp_stage_promotion_flag,
    b.stage_enter_date,
    b.stage_exit_date,
    b.stage_exit_date_or_today
from dwstage.dbo.temp_opportunity_monthly_history_and_today_table a
left join #opp_enter_exit_date_table_with_prom_flag b
ON a.opportunity_id = b.opportunity_id
    and [date] >= stage_enter_date 
    and [date] < stage_exit_date_or_today
;



-- TABLE NAME: dwbi.dbo.mart_opportunity_pipeline_history_dev
-- DESCRIPTION: pipeline table opps
-- recording only opps that have their stage change and the stage age when the change happens 


--production table
drop table if exists dwbi.dbo.mart_opportunity_pipeline_history_dev;
CREATE TABLE dwbi.dbo.mart_opportunity_pipeline_history_dev(
    full_date date,
    first_day_of_month date, 
    previous_first_day_of_month date,
    opportunity_id nvarchar(18),
    opp_number nvarchar(30),
    forecast_date date,
    forecast_region varchar(13),
    forecast_cvp nvarchar(255),
    forecast_svp nvarchar(255),
    opp_svp nvarchar(255),
    opp_cvp nvarchar(255),
    opp_region nvarchar(255),
    opp_region_rollup nvarchar(255),
    opp_revenue_type nvarchar(40),
    forecast_new_business_flag int,
    forecast_sales_team_exclusion_flag int,
    forecast_category_exclusion_flag int,
    forecast_product_family varchar(9),
    forecast_revenue_type nvarchar(255),
    forecast_commission_type nvarchar(255),
    forecast_product_type nvarchar(255),
    forecast_platforms nvarchar(255),
    forecast_category nvarchar(1300),
    forecast_category_pipeline_flag int,
    opp_stage nvarchar(40),
    forecast_value_amount numeric(38,10), 
    stage_age int,
    stage_enter_date date,
    opp_stage_promotion_flag int,
    stage_exit_date date,
    stage_exit_date_or_today date,
    opp_creation_date date,
    opp_source nvarchar(255),
    opp_customer_size_bucket varchar(10)
)
;
INSERT INTO dwbi.dbo.mart_opportunity_pipeline_history_dev(
    full_date,
    first_day_of_month, 
    previous_first_day_of_month,
    opportunity_id,
    opp_number,
    forecast_date,
    forecast_region,
    forecast_cvp,
    forecast_svp,
    opp_svp,
    opp_cvp,
    opp_region,
    opp_region_rollup,
    opp_revenue_type,
    forecast_new_business_flag,
    forecast_sales_team_exclusion_flag,
    forecast_category_exclusion_flag,
    forecast_product_family,
    forecast_revenue_type,
    forecast_commission_type,
    forecast_product_type,
    forecast_platforms,
    forecast_category,
    forecast_category_pipeline_flag,
    opp_stage,
    forecast_value_amount, 
    stage_age,
    stage_enter_date,
    opp_stage_promotion_flag,
    stage_exit_date,
    stage_exit_date_or_today,
    opp_creation_date,
    opp_source, 
    opp_customer_size_bucket
)
select 
    a.full_date,
    a.first_day_of_month, 
    a.previous_first_day_of_month,
    a.opportunity_id,
    a.opp_number,
    a.forecast_date,
    a.forecast_region,
    a.forecast_cvp,
    a.forecast_svp,
    d.opp_svp,
    d.opp_cvp,
    d.opp_region,
    d.opp_region_rollup,
    d.opp_revenue_type,
    a.forecast_new_business_flag,
    a.forecast_sales_team_exclusion_flag,
    a.forecast_category_exclusion_flag,
    a.forecast_product_family,
    a.forecast_revenue_type,
    a.forecast_commission_type,
    a.forecast_product_type,
    a.forecast_platforms,
    a.forecast_category,
    a.forecast_category_pipeline_flag,
    a.opp_stage,
    a.forecast_value_amount, 
    b.stage_age,
    b.stage_enter_date,
    b.opp_stage_promotion_flag,
    b.stage_exit_date,
    b.stage_exit_date_or_today,
    c.opp_creation_date,
    d.opp_source,
    d.opp_customer_size_bucket
from dwstage.dbo.temp_monthly_and_current_pipeline_value_and_stage a
left JOIN 
    dwstage.dbo.temp_opp_monthly_stage_age b
on a.FULL_DATE = b.[date] and a.opportunity_id = b.opportunity_id
left join 
    #opp_history_creation_date_table c
On a.opportunity_id = c.opportunity_id
-- left join 
--     #opp_source_and_customer_size_bucket_table d
-- on a.opportunity_id = d.opportunity_id
left join 
    #opp_unique_feature_table d
on a.opportunity_id = d.opportunity_id
;
select @RET_STATUS = @@error, @rowcount = @@ROWCOUNT


UPDATE dbo.DW_AUDIT SET DATE_END = GETDATE(), FLAG_STATUS = @RET_STATUS, cnt_rows = @rowcount
	WHERE DWID_PULL = @DWID_PULL AND NAME_OBJECT = 'mart_opportunity_pipeline_history_dev' 


-- ===========================================================================================================================================================================================================================================

-- TABLE NAME: dwstage.dbo.temp_opp_stage_change_history_summary_since_FY19
-- DESCRIPTION: table recording only opps stage change from SF_opportunity_table since FY19
drop table if exists dwstage.dbo.temp_opp_stage_change_history_summary_since_FY19;
select 
    opportunity_id,
    opp_number,
    prev_opp_stage,
    prev_opp_stage_order,
    date_of_stage_switch,
    current_opp_stage,
    current_opp_stage_order,
    next_opp_stage,
    next_opp_stage_order,
    cast((select first_day_of_month from DW.dbo.dim_date where cast(FULL_DATE as date) = cast(date_of_stage_switch as date)) as date) as first_day_of_month_of_stage_switch
into dwstage.dbo.temp_opp_stage_change_history_summary_since_FY19
from 
(
    select 
        opportunity_id, 
        opp_number,
        prev_opp_stage,
        prev_opp_stage_order,

        date_begin as date_of_stage_switch, 
        opp_stage as current_opp_stage,
        opp_stage_order as current_opp_stage_order,
        next_opp_stage,
        next_opp_stage_order
    from #opportunity_stage_rank_history
    where (opp_stage != prev_opp_stage or prev_opp_stage is null)
    group by opportunity_id, opp_number, date_begin, opp_stage, opp_stage_order, prev_opp_stage, prev_opp_stage_order
    , next_opp_stage, next_opp_stage_order
) a
where date_of_stage_switch >= '2018-02-01'
;


-- date table at the date level, for daily level of granularity for age in stage
drop table if exists #daily_date_table_since_FY19;
select
    cast(FULL_DATE as date) as full_date,
    cast(first_day_of_month as date) as first_day_of_month
into #daily_date_table_since_FY19
from DW.DBO.DIM_DATE
where cast(FULL_DATE as date) between '2018-02-01' and GETDATE()
;

create nonclustered index idx_full_date on #daily_date_table_since_FY19([full_date])


drop table if exists dwstage.dbo.temp_opportunity_history_table;
CREATE TABLE dwstage.dbo.temp_opportunity_history_table(
    opportunity_id nvarchar(18),
    opp_number nvarchar(30),
    [date] date,
    first_day_of_month date,
    opp_stage nvarchar(40),
    opp_revenue_type nvarchar(40),
    opp_region nvarchar(255),
    opp_region_rollup nvarchar(255),
    opp_svp nvarchar(255),
    opp_cvp nvarchar(255)
)
;
INSERT INTO dwstage.dbo.temp_opportunity_history_table(
    opportunity_id,
    opp_number,
    [date],
    first_day_of_month,
    opp_stage,
    opp_revenue_type,
    opp_region,
    opp_region_rollup,
    opp_svp,
    opp_cvp
)
SELECT 
    opp_summary_table.opportunity_id,
    opp_summary_table.opp_number,
    date_table.FULL_DATE as [date],
    date_table.first_day_of_month,
    opp_summary_table.opp_stage as opp_stage,
    opp_revenue_type,
    opp_region,
    opp_region_rollup,
    opp_svp,
    opp_cvp
from 
    #daily_date_table_since_FY19 as date_table
left JOIN #opp_history_summary_table as opp_summary_table
-- ======================================================== 
ON date_table.FULL_DATE >= opp_summary_table.DATE_BEGIN 
AND date_table.FULL_DATE < opp_summary_table.DATE_END
-- ========================================================
;

drop table if exists #stage_age_table;
select 
    a.[date],
    a.first_day_of_month,
    a.opportunity_id,
    a.opp_number, 
    a.opp_revenue_type,
    a.opp_region,
    a.opp_region_rollup,
    a.opp_svp,
    a.opp_cvp,
    a.opp_stage,
    datediff(day, b.stage_enter_date, a.[date])+1 as stage_age,
    b.stage_enter_date,
    b.next_stage_enter_date
into #stage_age_table
from dwstage.dbo.temp_opportunity_history_table a
inner join #opp_enter_exit_date_table_with_prom_flag b
ON a.opportunity_id = b.opportunity_id
    and [date] = DATEADD(day, -1, next_stage_enter_date)
group by     
    a.[date],
    a.first_day_of_month,
    a.opportunity_id,
    a.opp_number,
    a.opp_revenue_type,
    a.opp_region,
    a.opp_region_rollup,
    a.opp_svp,
    a.opp_cvp,
    a.opp_stage,
    b.stage_enter_date,
    b.next_stage_enter_date
;


--production table
drop table if exists dwbi.dbo.mart_opportunity_stage_progression_history_dev;
select 
    a.opportunity_id,
    a.opp_number,
    a.prev_opp_stage,
    a.prev_opp_stage_order,
    b.stage_enter_date as prev_opp_stage_enter_date,
    b.stage_age,
    a.date_of_stage_switch as current_opp_stage_enter_date,
    a.current_opp_stage,
    a.current_opp_stage_order,
    a.next_opp_stage,
    a.next_opp_stage_order,
    c.opp_revenue_type,
    c.opp_svp,
    c.opp_cvp,
    c.opp_region,
    c.opp_region_rollup,
    c.opp_customer_size_bucket,
    c.opp_source,
    d.opp_creation_date
into dwbi.dbo.mart_opportunity_stage_progression_history_dev
from dwstage.dbo.temp_opp_stage_change_history_summary_since_FY19 a with (nolock)
LEFT JOIN #stage_age_table b
ON a.opportunity_id = b.opportunity_id
and a.date_of_stage_switch = b.next_stage_enter_date
LEFT JOIN #opp_unique_feature_table c
ON a.opportunity_id = c.opportunity_id
left join #opp_history_creation_date_table d
On a.opportunity_id = d.opportunity_id
;

select @RET_STATUS = @@error, @rowcount = @@ROWCOUNT


UPDATE dbo.DW_AUDIT SET DATE_END = GETDATE(), FLAG_STATUS = @RET_STATUS, cnt_rows = @rowcount
	WHERE DWID_PULL = @DWID_PULL AND NAME_OBJECT = 'mart_opportunity_stage_progression_history_dev' 
GO


